This script will install or upgrade Octopy. Octopy will be installed into the folder
/opt/octopy. All known dependencies are also installed including Node.js and Mosquitto.
The script will also setup basic authentication of Mosquitto.

The script is only compatible with CentOS 9 and newer versions of Debian and derivates.

The install script takes a few arguments such as system id, daemon user, MQTT credentials etc. Use
  python3 install-octopy.py -h
for a complete list of available arguments.
