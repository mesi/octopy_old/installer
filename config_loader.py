import json
import os


DEFAULT_PATH = "./config/"

def load(path = DEFAULT_PATH):
    config = {}
    conf_files = {}
    if not os.path.exists(path):
        raise FileNotFoundError(f"Could not load config. Path does not exist: '{path}'")
    for root, dirs, files in os.walk(path):
        for file_name in files:
            if file_name.endswith(".json"):
                file = os.path.join(root, file_name)
                conf_files[file_name] = file
    for file_name in sorted(conf_files.keys()):
        # Load configuration file
        with open(conf_files[file_name], 'r') as conf_file:
            try:
                new_config = json.loads(conf_file.read())
                config = merge_configs(new_config, config)
            except json.decoder.JSONDecodeError as e:
                print(f"Error parsing config file {file_name}:")
                raise e
    if not 'prefix' in config['mqtt']:
        config['mqtt']['prefix'] = config['system id']
    config = post_process(config, config['mqtt']['prefix'])
    return config


def merge_configs(source, destination):
    for key, value in source.items():
        if isinstance(value, dict):
            # get node or create one
            node = destination.setdefault(key, {})
            merge_configs(value, node)
        else:
            destination[key] = value
    return destination


def post_process(config, prefix):
    for key, value in config.items():
        if isinstance(value, str):
            config[key] = value.replace('$PREFIX', prefix)
        elif isinstance(value, dict):
            post_process(value, prefix)
    return config
