#!/usr/bin/python3

import argparse
import socket
import sys
import subprocess
import os
import getpass
import shutil
import json
import urllib.request
import config_loader

INSTALLATION_PATH = "/opt/octopy"
DEFAULT_USER = 'daemon'

CONFIG_FOLDER = "config"
CONFIG_DB_FOLDER ="config-db"
LIBRARY_FOLDER = "lib"
DATASOURCES_FOLDER = "datasources"
ECS_GATEWAY_FOLDER = "ecs-gateways"

GIT_BASE_URL = "https://gitlab.esss.lu.se/mesi/octopy"

SYSTEM_PACKAGES_COMMON = [
    'git',
    'python3-pip',
    'screen',
    'ntp',
    'mosquitto'
]
SYSTEM_PACKAGES_DEBIAN = [
    'ntpstat'  # this is included in the NTP package on CentOS
]
SYSTEM_PACKAGES_CENTOS = [
    'gcc',
    'python3-devel'
]

PYTHON_PACKAGES_COMMON = [
    'paho-mqtt',
    'psutil',
    'pvapy',
    'numpy==1.19.4'
]

OCTOPY_REPOS = [
    'startup-manager',
    'storage-manager',
    'node-manager',
    'config-db',
    'ink',
    'mqtt-log-monitor',
    'lib/config_loader',
    'lib/links',
    'lib/secop',
    'lib/octopyapp',
    'datasources/tcp-request-response-hardware-gateway',
    'datasources/epics-datasource',
    'datasources/mqtt-datasource',
    'datasources/system-datasource',
    'datasources/test-datasource',
    'datasources/sequencer',
    'ecs-gateways/secop-ecs-gateway',
    'ecs-gateways/epics-ecs-gateway',
    'config/common'
]

CONFIG_DB_REPO = 'config-db'



class OctopyInstaller:
    def __init__(self, config):
        self.flags = {}
        self.config = config


    def start(self):
        self.setup_paths()
        self.update_self()
        self.check_priveliges()
        self.validate_config()
        self.check_os()
        self.user_verify()
        self.install_system_packages()
        self.install_nodejs()
        self.install_python_packages()
        self.install_octopy()
        #self.setup_ink()
        self.configure_octopy()
        self.change_ownership()
        self.setup_mosquitto()
        self.setup_as_daemon()
        self.start_octopy()


    def error(self, msg, fatal = False):
        print(f"ERROR: {msg}")
        if fatal:
            sys.exit()


    def setup_paths(self):
        self.base_path = INSTALLATION_PATH
        self.config_path = os.path.join(self.base_path, CONFIG_FOLDER)
        self.config_db_path = os.path.join(self.base_path, CONFIG_DB_FOLDER)


    def done_message(self):
        if self.flags['verbose']:
            print('DONE')
            print('=' * self.columns)
        else:
            print(f"\rDONE")


    def run_command(self, command, msg, cwd = None):
        args = command.split(' ')
        proc = subprocess.Popen(args, stderr=subprocess.PIPE, stdout=subprocess.PIPE, cwd = cwd)
        if msg:
            print(msg)
        if self.flags['verbose']:
            print(f"Executing: '{command}'")
        for line in iter(proc.stdout.readline, b''):
            line = line.decode('UTF-8').strip()
            line = line[:self.columns]  # Truncate to screen width
            line = line + (' ' * (self.columns - len(line)))
            if self.flags['verbose']:
                print(line)
            else:
                print(f"\r{line}", end = '')
        proc.wait()
        if not self.flags['verbose']:
            print('\r' + (' ' * (self.columns)), end = '')  # Clear line
        if proc.returncode != 0:
            for line in iter(proc.stderr.readline, b''):
                line = line.decode('UTF-8')
                print(f"ERROR: {line}", end='')
        self.done_message()


    def check_repo_exists(self, url):
        environment = dict(os.environ)   # Make a copy of the current environment
        environment['GIT_ASKPASS'] = '/bin/true'
        proc = subprocess.run(['git', 'ls-remote', url, '-q'], stdout=subprocess.PIPE, stderr=subprocess.STDOUT, env=environment)
        if proc.returncode == 0:
            return True
        else:
            return False


    def check_repo_is_updated(self, path):
        environment = dict(os.environ)   # Make a copy of the current environment
        environment['GIT_ASKPASS'] = '/bin/true'
        proc = subprocess.run(['git', 'fetch', '-q'], stdout=subprocess.PIPE, stderr=subprocess.STDOUT, env=environment, cwd=path)
        proc = subprocess.run(['git', 'status', '-uno'], stdout=subprocess.PIPE, stderr=subprocess.STDOUT, env=environment, cwd=path)
        output = proc.stdout.decode('utf-8').split('\n')
        if ('up to date' in output[1]) or ('nothing to commit' in output[1]):
            return True
        else:
            return False


    def update_self(self):
        up_to_date = self.check_repo_is_updated('./')
        if not up_to_date:
            self.error("ERROR: A new version of the install script is available. Please do a git pull and retry.", fatal = True)


    def check_priveliges(self):
        user = getpass.getuser()
        if user != 'root':
            self.error("Install script must be run as root or under sudo", fatal = True)


    def check_os(self):
        self.flags['Debian'] = False
        self.flags['CentOS'] = False
        proc = subprocess.run(['which', 'apt-get'], stdout=subprocess.PIPE, stderr=subprocess.STDOUT)
        if proc.returncode == 0:
            self.flags['Debian'] = True
            self.apt_path = proc.stdout.decode('ascii').strip()
        else:
            proc = subprocess.run(['which', 'yum'], stdout=subprocess.PIPE, stderr=subprocess.STDOUT)
            if proc.returncode == 0:
                self.flags['CentOS'] = True
                self.yum_path = proc.stdout.decode('ascii').strip()
            else:
                self.error("Could not find packagemanager 'yum' or 'apt-get'")
        tsize = os.get_terminal_size()
        self.columns = tsize.columns
        if self.columns < 10:
            self.columns = 80


    def validate_config(self):
        # Verbosity
        self.flags['verbose'] = False
        if self.config.get('verbose'):
            self.flags['verbose'] = True
        # MQTT Authentication
        self.flags['MQTT authentication'] = False
        if self.config['mqtt'].get('username') and self.config['mqtt'].get('password'):
            self.flags['MQTT authentication'] = True
        elif self.config['mqtt'].get('username'):
            self.error(f"MQTT username given, but no password", fatal = True)
        elif self.config['mqtt'].get('password'):
            self.error(f"MQTT password given, but no username", fatal = True)
        else:
            self.flags['MQTT authentication'] = False
        # System id
        self.flags['upgrade'] = False
        self.flags['reinstall'] = False
        if os.path.isdir(self.base_path):
            try:
                existing_config = config_loader.load(self.config_path)
                if existing_config.get('system id'):
                    self.config['old system id'] = existing_config['system id']
                answer = ""
                while answer == "" or not (answer in "uUiI"):
                    if self.config['old system id'] == self.config['system id'] or self.config['system id'] == '':
                        print(f"\nAn existing installation found")
                        answer = input(f"Do you want to (A)bort, (U)pgrade or do a clean (I)nstall erasing existing configuration? ")
                    else:
                        print(f"\nAn existing installation found with system id '{self.config['old system id']}'")
                        answer = input(f"Do you want to (A)bort, (U)pgrade with new system id '{self.config['system id']}' or do a clean (I)nstall erasing existing configuration? ")
                    if answer == "u" or answer == "U":
                        if self.config['system id'] == '':
                            self.config['system id'] = self.config['old system id']
                        self.flags['upgrade'] = True
                    elif answer == "a" or answer == "A":
                        sys.exit()
                    else:
                        self.flags['reinstall'] = True
            except Exception as err:
                print(err)
                pass
        if not self.config['system id']:
            hostname = socket.gethostname()
            host = hostname.split('.')[0]
            self.config['system id'] = host
        # User
        if not self.config.get('user'):
            self.config['user'] = DEFAULT_USER


    def user_verify(self):
        print("\nSystem information")
        if self.flags['CentOS']:
            print("    OS:                  CentOS")
            print("    Package manager:     yum")
        elif self.flags['Debian']:
            print("    OS:                  Debian")
            print("    Package manager:     apt-get")

        print("\nThe system will install with the following parameters")
        print(f"    System ID:           {self.config['system id']}")
        if self.flags['upgrade']:
            print(f"    Old system ID:       {self.config['old system id']}")
            print(f"    Upgrade:             YES")
        else:
            print(f"    Upgrade:             NO")
        print(f"    Running as user:     {self.config['user']}")
        if self.flags['MQTT authentication']:
            print(f"    MQTT Authentication: YES")
            print(f"    MQTT Username:       {self.config['mqtt']['username']}")
            print(f"    MQTT Password:       {self.config['mqtt']['password']}")
        else:
            print(f"    MQTT Authentication: NO")
            print()
            print(f"    *** WARNING! THIS IS UNSECURE! ***")
            print(f"    *** MQTT Authentication should ALWAYS be enabled! ***")
            print()
            print()
        if self.flags['reinstall']:
            print()
            print("WARNING! AN EXISTING INSTALLATION WILL BE OVERWRITTEN!")
            print("ALL EXISTING CONFIGURATION WILL BE LOST!")
        print()
        answer = input("Proceed (Y/N)? ")
        if not answer == "Y" and not answer == "y":
            sys.exit()


    def install_system_packages(self):
        if self.flags['Debian']:
            for package in SYSTEM_PACKAGES_DEBIAN:
                self.install_system_package(package)
        elif self.flags['CentOS']:
            for package in SYSTEM_PACKAGES_CENTOS:
                self.install_system_package(package)
        for package in SYSTEM_PACKAGES_COMMON:
            self.install_system_package(package)


    def install_system_package(self, package):
        msg = f"Installing {package}"
        if self.flags['Debian']:
            command = f"{self.apt_path} install -y {package}"
            self.run_command(command, msg)
        elif self.flags['CentOS']:
            command = f"{self.yum_path} install -y {package}"
            self.run_command(command, msg)


    def install_nodejs(self):
        if self.flags['Debian']:
            command = f"{self.apt_path} install -y nodejs npm"
            self.run_command(command, 'Installing Node.js')
        elif self.flags['CentOS']:
            print("Downloading setup script for Node.js")
            url = 'https://rpm.nodesource.com/setup_16.x'
            response = urllib.request.urlopen(url)
            data = response.read()
            text = data.decode('ascii')
            with open('setup_nodejs.sh', 'w') as f:
                f.write(text)
            self.done_message()
            command = 'bash -E setup_nodejs.sh'
            self.run_command(command, 'Adding source for Node.js')
            os.remove('setup_nodejs.sh')
            command = f'{self.yum_path} install -y nodejs'
            self.run_command(command, 'Installing Node.js')


    def install_python_packages(self):
        for package in PYTHON_PACKAGES_COMMON:
            self.install_python_package(package)


    def install_python_package(self, package):
        command = f"pip3 install {package}"
        self.run_command(command, f"Installing Python package {package}")


    def checkout_repo(self, repo):
        destination = os.path.join(self.base_path, repo)
        url = os.path.join(GIT_BASE_URL, repo) + '.git'
        if not self.check_repo_exists(url):
            print(f"ERROR: Repo {url} does not exist or is inaccessible")
            return
        if not  self.flags['upgrade'] and os.path.isdir(destination):
            shutil.rmtree(destination)
        if os.path.isdir(destination):
            # Repo exists already, update
            command = f"git pull"
            self.run_command(command, f"Updating {repo} in {destination}", cwd = destination)
        else:
            # Checkout repo
            if ('/' in repo):
                (subpath, repo) = repo.rsplit('/', 1)
                parentpath = os.path.join(self.base_path, subpath)
            else:
                parentpath = self.base_path
            if not os.path.isdir(parentpath):
                os.makedirs(parentpath)
            command = f"git clone {url} {destination}"
            self.run_command(command, f"Installing {repo} into {destination}")


    def install_octopy(self):
        if not self.flags['upgrade']:
            if os.path.isdir(self.config_path):
                shutil.rmtree(self.config_path)
            if os.path.isdir(self.config_db_path):
                shutil.rmtree(self.config_db_path)
        for repo in OCTOPY_REPOS:
            self.checkout_repo(repo)


    def setup_ink(self):
        ink_path = os.path.join(self.base_path, 'ink')
        command = f"npm update"
        self.run_command(command, f"Installing Node.js dependencies for GUI", cwd = ink_path)


    def configure_octopy(self):
        config = {}
        if self.flags['upgrade']:
            old_config_path = os.path.join(self.config_path, self.config['old system id'])
            print(f"Reading old configuration from {old_config_path}")
            try:
                config = config_loader.load(old_config_path)
                shutil.rmtree(old_config_path)
                self.done_message()
            except Exception as err:
                print(f"ERROR: {err}")
        config['system id'] = self.config['system id']
        if not ('mqtt' in config):
            config['mqtt'] = {}
        config['mqtt']['prefix'] = self.config['system id']
        if self.flags['MQTT authentication']:
            config['mqtt']['username'] = self.config['mqtt']['username']
            config['mqtt']['password'] = self.config['mqtt']['password']
        else:
            if 'username' in config['mqtt']:
                config['mqtt']['username'] = None
            if 'password' in config['mqtt']:
                config['mqtt']['password'] = None
        config_path = os.path.join(self.config_path, self.config['system id'])
        config_file = os.path.join(config_path, f"50-{self.config['system id']}.json")
        if not os.path.isdir(config_path):
            os.mkdir(config_path)
        conf_data = json.dumps(config, indent = 4)
        print(f"Writing new configuration to {config_file}")
        with open(config_file, 'w') as f:
            f.write(conf_data)
        self.done_message()
        # Configuration database
        if not self.flags['upgrade'] and os.path.isdir(self.config_db_path):
            print(f"Deleting existing configuration database in {self.config_db_path}")
            shutil.rmtree(self.config_db_path)
            self.done_message()
        self.checkout_repo(CONFIG_DB_REPO)


    def change_ownership(self):
        owner = self.config['user']
        group = owner
        for dirpath, dirnames, filenames in os.walk(self.base_path):
            shutil.chown(dirpath, owner, group)
            for filename in filenames:
                shutil.chown(os.path.join(dirpath, filename), owner, group)


    def setup_as_daemon(self):
        sysd_script = ("[Unit]\n"
                       "Description=Octopy\n"
                       "After=network.target\n"
                       "StartLimitIntervalSec=0\n"
                       "\n"
                       "[Service]\n"
                       "Type=simple\n"
                       "Restart=always\n"
                       "RestartSec=1\n"
                       f"User={self.config['user']}\n"
                       f"WorkingDirectory={os.path.join(self.base_path, 'startup-manager')}\n"
                       f"ExecStart={os.path.join(self.base_path, 'startup-manager/start')}\n"
                       "\n"
                       "[Install]\n"
                       "WantedBy=multi-user.target\n")
        path = '/etc/systemd/system/octopy.service'
        with open(path, 'w') as f:
            f.write(sysd_script)


    def setup_mosquitto(self):
        passwd_file = '/etc/mosquitto/passwd'
        conf_string = f"password_file {passwd_file}\n"
        # Configure/remove Mosquitto authentication
        print("Configuring Mosquitto")
        if self.flags['CentOS']:
            mosquitto_conf = '/etc/mosquitto/mosquitto.conf'
            conf = []
            with open(mosquitto_conf, 'r') as f:
                for line in f:
                    if not ('password_file' in line):
                        conf.append(line)
            if self.flags['MQTT authentication']:
                conf.append(conf_string)
            with open(mosquitto_conf, 'w') as f:
                for line in conf:
                    f.write(line)
        elif self.flags['Debian']:
            mosquitto_conf = '/etc/mosquitto/conf.d/auth.conf'
            if self.flags['MQTT authentication']:
                with open(mosquitto_conf, 'w') as f:
                    f.write(conf_string)
            elif os.path.isfile(mosquitto_conf):
                os.remove(mosquitto_conf)
        self.done_message()
        # Create/remove authentication file
        if self.flags['MQTT authentication']:
            print("Creating Mosquitto authentication file")
            passwd_content = f"{self.config['mqtt']['username']}:{self.config['mqtt']['password']}"
            with open(passwd_file, 'w') as f:
                f.write(passwd_content)
            self.done_message()
            command = f"mosquitto_passwd -U {passwd_file}"
            self.run_command(command, "Encrypting Mosquitto password")
        else:
            if os.path.isfile(passwd_file):
                os.remove(passwd_file)
        # Start/restart Mosquitto
        command = f"systemctl enable mosquitto.service"
        self.run_command(command, "Setting Mosquitto to autostart on boot")
        command = f"systemctl restart mosquitto.service"
        self.run_command(command, "(Re-)Starting Mosquitto")



    def start_octopy(self):
       self.run_command("systemctl daemon-reload", "Reloading Systemd scripts")
       self.run_command("systemctl restart octopy.service", "Starting Ocotpy as daemon")



if __name__ == '__main__':

    arg_parser = argparse.ArgumentParser()
    arg_parser.add_argument('-o', '--user',      help="User that Octopy will run as", type=str, default="")
    arg_parser.add_argument('-u', '--mqttuser',  help="MQTT username", type=str, default="")
    arg_parser.add_argument('-p', '--mqttpass',  help="MQTT password", type=str, default="")
    arg_parser.add_argument('-i', '--system-id', help="System ID",type=str, default="")
    arg_parser.add_argument('-v', '--verbose',   help="Show more output", action='store_true')
    args = arg_parser.parse_args()
    config = {
        'system id': args.system_id,
        'mqtt': {}
    }
    if args.mqttuser:
        config['mqtt']['username'] = args.mqttuser
    if args.mqttpass:
        config['mqtt']['password'] = args.mqttpass
    if args.user:
        config['user'] = args.user
    config['verbose'] = True if args.verbose else False

    installer = OctopyInstaller(config)
    installer.start()
